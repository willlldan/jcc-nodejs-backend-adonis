var express = require("express");
var router = express.Router();

// Controller
const UserController = require("../controllers/users");

/* GET users listing. */
router.get("/karyawan", UserController.findAll);
router.post("/register", UserController.register);
router.post("/login", UserController.login);
router.post("/karyawan/:name/siswa", UserController.addSiswa);

module.exports = router;
