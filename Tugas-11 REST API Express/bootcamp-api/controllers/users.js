const fs = require("fs");

class UserController {
  // =============================================================
  // Release 0 Register karyawan (Callback)
  // =============================================================
  static register(req, res) {
    fs.readFile("data.json", (err, data) => {
      if (err) {
        res.status(400).json({ errors: "Error membaca data" });
      } else {
        let existingData = JSON.parse(data);
        let { users } = existingData;
        let { name, role, password } = req.body;
        let newUser = { name, role, password, isLogin: false };
        users.push(newUser);

        let newData = { ...existingData, users };

        fs.writeFile("data.json", JSON.stringify(newData, null, 2), (err) => {
          if (err) {
            res.status(400).json({ errors: "Error menyimpan data" });
          } else {
            res.status(200).json({ messages: "Berhasil register" });
          }
        });
      }
    });
  }

  // =============================================================
  // Release 1 Show All Karyawan
  // =============================================================
  static findAll(req, res) {
    fs.readFile("data.json", (err, data) => {
      if (err) {
        res.status(400).json({ errors: "Error membaca data" });
      } else {
        let realData = JSON.parse(data);
        res
          .status(200)
          .json({ messages: "Berhasil get data karyawan", data: realData });
      }
    });
  }

  // =============================================================
  // Release 2 Login
  // =============================================================
  static login(req, res) {
    fs.readFile("data.json", (err, data) => {
      if (err) {
        res.status(400).json({ errors: "Error membaca data" });
      } else {
        let existingData = JSON.parse(data);
        let { users } = existingData;
        let { name, password } = req.body;

        let karyawan = users.find(
          (item) =>
            item.name.toLowerCase() == name.toLowerCase() &&
            item.password == password
        );

        if (karyawan == undefined) {
          res
            .status(400)
            .json({ messages: "name dan/atau password tidak sesuai" });
        } else {
          karyawan.isLogin = true;

          let newData = { ...existingData, users };

          fs.writeFile("data.json", JSON.stringify(newData, null, 2), (err) => {
            if (err) {
              res.status(400).json({ errors: "Error menyimpan data" });
            } else {
              res.status(200).json({ messages: "Berhasil Login" });
            }
          });
        }
      }
    });
  }

  // =============================================================
  // Release 3 Add Siswa
  // =============================================================

  static addSiswa(req, res) {
    let trainer = req.params.name;

    fs.readFile("data.json", (err, data) => {
      if (err) {
        res.status(400).json({ errors: "Error membaca data" });
        return;
      }

      //   get data from json
      let existingData = JSON.parse(data);
      let { users } = existingData;
      let { name, kelas } = req.body;
      let karyawan = users.find(
        (item) => item.name.toLowerCase() == trainer.toLowerCase()
      );
      if (karyawan == undefined) {
        res.status(400).json({ messages: "name trainer tidak terdaftar" });
        return;
      }

      //   cek login
      if (!karyawan.isLogin) {
        res.status(400).json({ messages: "Silahkan login terlebih dahulu" });
        return;
      }

      //   cek role (harus trainer)
      if (karyawan.role != "trainer") {
        res
          .status(400)
          .json({ messages: "Hanya trainer yang boleh menambahkan siswa" });
        return;
      }

      //   cek array siswa sudah ada
      if (karyawan.students == null) {
        console.log("kesini");
        karyawan.students = [];
      }

      //  tambah siswa
      let student = { name, kelas };
      karyawan.students.push(student);

      let newData = { ...existingData, users };
      fs.writeFile("data.json", JSON.stringify(newData, null, 2), (err) => {
        if (err) {
          res.status(400).json({ errors: "Error menyimpan data" });
        } else {
          res.status(200).json({ messages: "Berhasil add siswa" });
        }
      });
    });
  }
}

module.exports = UserController;
