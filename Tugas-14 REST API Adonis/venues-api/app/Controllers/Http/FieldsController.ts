import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import Field from 'App/Models/Field'

export default class FieldsController {
  public async index({response}: HttpContextContract) {
    let fields = await Field.all()
    response.status(200).json({messages: "Successfully get data", data: fields})
  }

  public async store({request, response}: HttpContextContract) {
    await request.validate(CreateFieldValidator)

    let newField = new Field();
    newField.name = request.input('name')
    newField.type = request.input('type')
    newField.venue_id = request.input('venue_id')

    try {
      await newField.save()
      response.created({messages: "Successfully Added"})
    } catch (error) {
      response.badRequest(error)
    }
  }

  public async show({response, params}: HttpContextContract) {
    let field = await Field.findOrFail(params.id)
    response.status(200).json({messages: "success get field", data : field})
  }

  public async update({request, response, params}: HttpContextContract) {

    let field = await Field.findOrFail(params.id)
    field.name = request.input('name')
    field.type = request.input('type')
    field.venue_id = request.input('venue_id')

    await field.save()
    response.status(200).json({messages: "Successfully Updated!"})
  }

  public async destroy({params, response}: HttpContextContract) {
    await (await Field.findOrFail(params.id)).delete()
    response.status(200).json({messages: "Successfully Deleted!"})
  }
}
