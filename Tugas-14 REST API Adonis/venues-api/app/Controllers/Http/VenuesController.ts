import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import BookingVenueValidator from 'App/Validators/BookingVenueValidator'
import Venue from 'App/Models/Venue'


export default class VenuesController {

    public async index({response}:HttpContextContract) {
        let venues = await Venue.all()
        response.status(200).json({messages: "success get venues", data : venues})
    }


    public async store({request, response} : HttpContextContract) {
        await request.validate(CreateVenueValidator)

        let newVenue = new Venue();
        newVenue.name = request.input('name')
        newVenue.address = request.input('address')
        newVenue.phone = request.input('phone')

        try {
            await newVenue.save()
            response.created({messages: "Successfully added!"})
        } catch (error) {
            response.badRequest(error)
        }
    }

    public async show({params, response}:HttpContextContract) {
        let venues = await Venue.findByOrFail('id', params.id)
        response.status(200).json({messages: "success get venues", data : venues})
    }

    public async update({request, response, params}:HttpContextContract) {
        let venue = await Venue.findOrFail(params.id)
        venue.name = request.input('name')
        venue.address = request.input('address')
        venue.phone = request.input('phone')

        try {
            venue.save()
            response.status(200).json({messages: "Successfully Updated!"})
        } catch (error) {
            response.badRequest(error)
        }
       
    }

    public async destroy({params, response}: HttpContextContract) {
        (await Venue.findOrFail(params.id)).delete()
        response.status(200).json({messages: "Successfully Deleted!"})
    }


    public async booking({request, response}: HttpContextContract) {

        const payload = await request.validate(BookingVenueValidator)
        response.status(200).json(payload)

    }


}
