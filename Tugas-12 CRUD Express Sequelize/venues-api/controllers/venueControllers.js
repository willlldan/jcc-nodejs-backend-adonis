const { Venue } = require("../models");

class VenueController {
  static async findAll(req, res) {
    let venues = await Venue.findAll();
    res.status(200).json({
      status: "success",
      data: venues,
    });
  }

  static async store(req, res) {
    //   get request
    let { name, address, phone } = req.body;

    // store data use model Venue
    await Venue.create({ name, address, phone });

    res
      .status(200)
      .json({ status: "success", messages: "Venue successfully added" });
  }

  static async show(req, res) {
    let { id } = req.params;

    let venue = await Venue.findByPk(id);

    res.status(200).json({
      status: "success",
      data: venue,
    });
  }

  static async update(req, res) {
    //   get request
    let { name, address, phone } = req.body;
    let { id } = req.params;

    await Venue.update(
      {
        name,
        address,
        phone,
      },
      {
        where: {
          id,
        },
      }
    );

    res.status(200).json({
      status: "success",
      messages: `Venue with id: ${id} successfully updated`,
    });
  }

  static async destroy(req, res) {
    await Venue.destroy({
      where: { id: req.params.id },
    });

    res.status(200).json({
      status: "success",
      messages: `Venue with id: ${req.params.id} successfully deleted`,
    });
  }
}

module.exports = VenueController;
