var express = require("express");
const { route } = require(".");
var router = express.Router();

// import controller
const VenueController = require("../controllers/venueControllers");

router.get("/venues", VenueController.findAll);
router.post("/venues", VenueController.store);
router.get("/venues/:id", VenueController.show);
router.put("/venues/:id", VenueController.update);
router.delete("/venues/:id", VenueController.destroy);

module.exports = router;
