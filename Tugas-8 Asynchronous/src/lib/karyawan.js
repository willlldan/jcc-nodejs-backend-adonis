export default class Karyawan {
  constructor(name, password, role) {
    (this.name = name),
      (this.password = password),
      (this.role = role),
      (this.isLogin = false);
    this.student = [];
  }

  addSiswa(siswa) {
    this.student.push(siswa);
  }

  get kName() {
    return this.name;
  }

  set kName(name) {
    this.name = name;
  }

  get kPassword() {
    return this.password;
  }

  set kPassword(password) {
    this.password = password;
  }
  get kRole() {
    return this.role;
  }

  set kRole(role) {
    this.role = role;
  }

  get kIsLogin() {
    return this.isLogin;
  }

  set kIsLogin(isLogin) {
    this.isLogin = isLogin;
  }

  get kStudent() {
    return this.student;
  }

  set kStudent(student) {
    this.student = student;
  }
}
