import Karyawan from "./karyawan";
import Siswa from "./siswa";
import fs from "fs";

const path = "./dist/lib/data.json";

// =====================================================
// Release 0 (Register Karyawan)
// =====================================================

const register = (name, password, role) => {
  const newKaryawan = new Karyawan(name, password, role);
  inputToJson(newKaryawan);
  return `Berhasil register. Selamat datang ${name}`;
};

const inputToJson = (data) => {
  const file = fs.readFileSync(path, "utf-8");
  const karyawan = JSON.parse(file);

  karyawan.push(data);

  fs.writeFileSync(path, JSON.stringify(karyawan, null, 2));
};

// =====================================================
// Release 1 (Login with promise)
// =====================================================

const login = (name, password) => {
  fs.promises
    .readFile(path)
    .then((data) => {
      let realData = JSON.parse(data);
      let karyawan = realData.find(
        (item) =>
          item.name.toLowerCase() == name.toLowerCase() &&
          item.password == password
      );
      karyawan.isLogin = true;
      return fs.promises.writeFile(path, JSON.stringify(realData, null, 2));
    })
    .then(() => console.log("Berhasil Login"))
    .catch(() => {
      console.log("Gagal Login");
    });
};

// =====================================================
// Release 2 (Add Siswa)
// =====================================================

const addSiswa = async (siswa, trainer) => {
  try {
    let dataRead = await fs.promises.readFile(path);
    let realData = JSON.parse(dataRead);
    let findTrainer = realData.find((k) => k.name == trainer && k.isLogin);
    findTrainer["student"].push(new Siswa(siswa));

    await fs.promises.writeFile(path, JSON.stringify(realData, null, 2));
    console.log("Berhasil ditambahkan");
  } catch (error) {
    console.log("Trainer tidak terdaftar / Trainer belum login");
  }
};

export const menu = (key) => {
  let param = key[3].split(",");
  switch (key[2]) {
    case "register":
      console.log(register(param[0], param[1], param[2]));
      break;
    case "login":
      login(param[0], param[1]);
      break;
    case "addSiswa":
      addSiswa(param[0], param[1]);
      break;
    default:
      console.log("Perintah yang anda masukan tidak sesuai");
      console.log("List functions : ");
      console.log("node dist register <nama>,<password>,<role>");
      console.log("node dist login <nama>,<password>");
      console.log("node dist addSiswa <siswa>,<trainer>");

      break;
  }
};
