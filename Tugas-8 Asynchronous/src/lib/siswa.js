export default class Siswa {
  constructor(name) {
    this.name = name;
  }

  get sName() {
    return this.name;
  }

  set sName(name) {
    return this.name;
  }
}
