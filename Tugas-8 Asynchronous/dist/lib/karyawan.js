"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Karyawan = /*#__PURE__*/function () {
  function Karyawan(name, password, role) {
    (0, _classCallCheck2["default"])(this, Karyawan);
    this.name = name, this.password = password, this.role = role, this.isLogin = false;
    this.student = [];
  }

  (0, _createClass2["default"])(Karyawan, [{
    key: "addSiswa",
    value: function addSiswa(siswa) {
      this.student.push(siswa);
    }
  }, {
    key: "kName",
    get: function get() {
      return this.name;
    },
    set: function set(name) {
      this.name = name;
    }
  }, {
    key: "kPassword",
    get: function get() {
      return this.password;
    },
    set: function set(password) {
      this.password = password;
    }
  }, {
    key: "kRole",
    get: function get() {
      return this.role;
    },
    set: function set(role) {
      this.role = role;
    }
  }, {
    key: "kIsLogin",
    get: function get() {
      return this.isLogin;
    },
    set: function set(isLogin) {
      this.isLogin = isLogin;
    }
  }, {
    key: "kStudent",
    get: function get() {
      return this.student;
    },
    set: function set(student) {
      this.student = student;
    }
  }]);
  return Karyawan;
}();

exports["default"] = Karyawan;