"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Siswa = /*#__PURE__*/function () {
  function Siswa(name) {
    (0, _classCallCheck2["default"])(this, Siswa);
    this.name = name;
  }

  (0, _createClass2["default"])(Siswa, [{
    key: "sName",
    get: function get() {
      return this.name;
    },
    set: function set(name) {
      return this.name;
    }
  }]);
  return Siswa;
}();

exports["default"] = Siswa;