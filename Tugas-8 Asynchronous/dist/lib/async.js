"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.menu = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _karyawan = _interopRequireDefault(require("./karyawan"));

var _siswa = _interopRequireDefault(require("./siswa"));

var _fs = _interopRequireDefault(require("fs"));

var path = "./dist/lib/data.json"; // =====================================================
// Release 0 (Register Karyawan)
// =====================================================

var register = function register(name, password, role) {
  var newKaryawan = new _karyawan["default"](name, password, role);
  inputToJson(newKaryawan);
  return "Berhasil register. Selamat datang ".concat(name);
};

var inputToJson = function inputToJson(data) {
  var file = _fs["default"].readFileSync(path, "utf-8");

  var karyawan = JSON.parse(file);
  karyawan.push(data);

  _fs["default"].writeFileSync(path, JSON.stringify(karyawan, null, 2));
}; // =====================================================
// Release 1 (Login with promise)
// =====================================================


var login = function login(name, password) {
  _fs["default"].promises.readFile(path).then(function (data) {
    var realData = JSON.parse(data);
    var karyawan = realData.find(function (item) {
      return item.name.toLowerCase() == name.toLowerCase() && item.password == password;
    });
    karyawan.isLogin = true;
    return _fs["default"].promises.writeFile(path, JSON.stringify(realData, null, 2));
  }).then(function () {
    return console.log("Berhasil Login");
  })["catch"](function () {
    console.log("Gagal Login");
  });
}; // =====================================================
// Release 2 (Add Siswa)
// =====================================================


var addSiswa = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(siswa, trainer) {
    var dataRead, realData, findTrainer;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _fs["default"].promises.readFile(path);

          case 3:
            dataRead = _context.sent;
            realData = JSON.parse(dataRead);
            findTrainer = realData.find(function (k) {
              return k.name == trainer && k.isLogin;
            });
            findTrainer["student"].push(new _siswa["default"](siswa));
            _context.next = 9;
            return _fs["default"].promises.writeFile(path, JSON.stringify(realData, null, 2));

          case 9:
            console.log("Berhasil ditambahkan");
            _context.next = 15;
            break;

          case 12:
            _context.prev = 12;
            _context.t0 = _context["catch"](0);
            console.log("Trainer tidak terdaftar / Trainer belum login");

          case 15:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 12]]);
  }));

  return function addSiswa(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var menu = function menu(key) {
  var param = key[3].split(",");

  switch (key[2]) {
    case "register":
      console.log(register(param[0], param[1], param[2]));
      break;

    case "login":
      login(param[0], param[1]);
      break;

    case "addSiswa":
      addSiswa(param[0], param[1]);
      break;

    default:
      console.log("Perintah yang anda masukan tidak sesuai");
      console.log("List functions : ");
      console.log("node dist register <nama>,<password>,<role>");
      console.log("node dist login <nama>,<password>");
      console.log("node dist addSiswa <siswa>,<trainer>");
      break;
  }
};

exports.menu = menu;