var looping = require("./looping");

var args = process.argv;

switch (args[2]) {
  case "while":
    looping.whileLoop();
    break;
  case "for":
    looping.forLoop();
    break;
  case "persegiPanjang":
    looping.persegi(args[3], args[4]);
    break;
  case "tangga":
    looping.tangga(args[3]);
    break;
  case "catur":
    looping.catur(args[3]);
    break;
  default:
    console.log("Perintah yang anda masukan tidak sesuai");
    console.log("List functions : ");
    console.log("node looping while");
    console.log("node looping for");
    console.log("node looping persegiPanjang <panjang> <lebar>");
    console.log("node looping tangga <sisi>");
    console.log("node looping catur <sisi>");
}
