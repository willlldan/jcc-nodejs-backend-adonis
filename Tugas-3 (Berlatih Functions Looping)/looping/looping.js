// Soal No 1 - Looping while

function whileLoop() {
  console.log("Looping Pertama");
  var i = 0;
  while (i < 20) {
    if (i % 2 == 0) console.log(`${i} - I love Coding`);
    i++;
  }

  console.log("Looping Pertama");
  while (i > 0) {
    if (i % 2 == 0) console.log(`${i} - I will become a backend developer`);
    i--;
  }
}

// Soal no 2 - Looping For
function forLoop() {
  for (var i = 1; i <= 20; i++) {
    if (i == 0) continue;
    if (i % 3 == 0 && i % 2 != 0) {
      console.log(`${i} - I Love Coding`);
    } else if (i % 2 == 0) {
      console.log(`${i} - Berkualitas`);
    } else {
      console.log(`${i} - Santai`);
    }
  }
}

// Soal no 3 - persegi panjang
function persegi(panjang, lebar) {
  for (var i = 0; i < lebar; i++) {
    var build = "";
    for (let j = 0; j < panjang; j++) {
      build += "*";
    }
    console.log(build);
  }
}

// Soal No 4 - Tangga
function tangga(sisi) {
  for (var i = 0; i < sisi; i++) {
    var build = "";
    for (var j = 0; j <= i; j++) {
      build += "#";
    }
    console.log(build);
  }
}

//  Soal No 5 - Papan catur
function catur(sisi) {
  for (var i = 0; i < sisi; i++) {
    var build = "";
    for (let j = 0; j < sisi; j++) {
      if (j % 2 == i % 2) {
        build += " ";
      } else {
        build += "#";
      }
    }
    console.log(build);
  }
}

module.exports = { whileLoop, forLoop, persegi, tangga, catur };
