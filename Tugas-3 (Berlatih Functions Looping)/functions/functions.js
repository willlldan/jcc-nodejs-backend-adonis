// Soal No 1
function teriak() {
  return "Hallooooo JCC!!!";
}

// Soal No 2
function kalikan(angka1, angka2) {
  return angka1 * angka2;
}

// Soal No 3
function kenalan(name, age, address, hobby) {
  return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address} dan saya punya hobby yaitu ${hobby}!!`;
}

module.exports = { teriak, kalikan, kenalan };
