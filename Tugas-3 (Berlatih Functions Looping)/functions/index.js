var func = require("./functions");

var args = process.argv;

switch (args[2]) {
  case "teriak":
    console.log(func.teriak());
    break;
  case "kalikan":
    console.log(func.kalikan(args[3], args[4]));
    break;
  case "kenalan":
    console.log(func.kenalan(args[3], args[4], args[5], args[6]));
    break;
  default:
    console.log("Perintah yang anda masukan tidak sesuai");
    console.log("List functions : ");
    console.log("node functions teriak");
    console.log("node functions kalikan <angka 1> <angka 2>");
    console.log("node functions kenalan <name> <age> <address> <hobby>");
    break;
}
