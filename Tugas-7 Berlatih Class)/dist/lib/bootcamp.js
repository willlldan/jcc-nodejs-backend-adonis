"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _kelas = _interopRequireDefault(require("./kelas"));

var _student = _interopRequireDefault(require("./student"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name) {
    _classCallCheck(this, Bootcamp);

    this.name = name;
    this.classes = [];
  }

  _createClass(Bootcamp, [{
    key: "createClass",
    value: function createClass(name, level, instruktor) {
      this.classes.push(new _kelas["default"](name, level, instruktor));
    }
  }, {
    key: "register",
    value: function register(kelas, newStudent) {
      this.classes.find(function (findClass) {
        return findClass.name == kelas;
      }).addStudent(newStudent);
    }
  }, {
    key: "runBatch",
    value: function runBatch() {
      var _this = this;

      this.classes.forEach(function (kelas) {
        kelas.student.forEach(function (st, i, students) {
          for (var j = 0; j < 4; j++) {
            var score = Math.floor(Math.random() * (100 - 50) + 50);
            st.score.push(score);
            st.finalScore += score;

            if (j == 3) {
              st.finalScore = Math.floor(st.finalScore / st.score.length);
            }
          }
        });
        console.log("graduated from ".concat(kelas.name, " :"), _this.graduate(kelas.name));
      });
    }
  }, {
    key: "graduate",
    value: function graduate(kelas) {
      return this.classes.find(function (findClass) {
        return findClass.name == kelas;
      }).graduate();
    } //   ==========================================
    // Setter & Getter
    //   ==========================================

  }, {
    key: "bclasses",
    get: function get() {
      return this.classes;
    },
    set: function set(classes) {
      this.classes = classes;
    }
  }, {
    key: "bname",
    get: function get() {
      return this.name;
    },
    set: function set(name) {
      this.name = name;
    }
  }]);

  return Bootcamp;
}();

exports["default"] = Bootcamp;