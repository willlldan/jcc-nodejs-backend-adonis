"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _student = _interopRequireDefault(require("./student"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(name, level, instruktor) {
    _classCallCheck(this, Kelas);

    this.name = name, this.student = [], this.level = level, this.instruktor = instruktor;
  }

  _createClass(Kelas, [{
    key: "addStudent",
    value: function addStudent(student) {
      this.student.push(student);
    }
  }, {
    key: "graduate",
    value: function graduate() {
      var graduates = {
        participant: [],
        completed: [],
        mastered: []
      };
      this.student.forEach(function (st) {
        if (st.finalScore > 85) {
          graduates.mastered.push(st);
        } else if (st.finalScore > 65) {
          graduates.mastered.push(st);
        } else {
          graduates.mastered.push(st);
        }
      });
      return graduates;
    }
  }, {
    key: "kname",
    get: function get() {
      return this.name;
    },
    set: function set(name) {
      this.name = name;
    }
  }, {
    key: "klevel",
    get: function get() {
      return this.level;
    },
    set: function set(level) {
      this.level = level;
    }
  }, {
    key: "kinstruktor",
    get: function get() {
      return this.instruktor;
    },
    set: function set(instruktor) {
      this.instruktor = instruktor;
    }
  }]);

  return Kelas;
}();

exports["default"] = Kelas;