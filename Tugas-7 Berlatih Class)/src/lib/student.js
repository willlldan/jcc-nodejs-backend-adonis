export default class Student {
  constructor(name) {
    (this.name = name), (this.score = []), (this.finalScore = 0);
  }

  get sname() {
    return this.name;
  }

  set sname(name) {
    this.name = name;
  }
}
