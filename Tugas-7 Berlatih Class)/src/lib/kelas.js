import Student from "./student";

export default class Kelas {
  constructor(name, level, instruktor) {
    (this.name = name),
      (this.student = []),
      (this.level = level),
      (this.instruktor = instruktor);
  }

  addStudent(student) {
    this.student.push(student);
  }

  graduate() {
    let graduates = {
      participant: [],
      completed: [],
      mastered: [],
    };
    this.student.forEach((st) => {
      if (st.finalScore > 85) {
        graduates.mastered.push(st);
      } else if (st.finalScore > 65) {
        graduates.mastered.push(st);
      } else {
        graduates.mastered.push(st);
      }
    });

    return graduates;
  }

  get kname() {
    return this.name;
  }

  set kname(name) {
    this.name = name;
  }

  get klevel() {
    return this.level;
  }

  set klevel(level) {
    this.level = level;
  }

  get kinstruktor() {
    return this.instruktor;
  }

  set kinstruktor(instruktor) {
    this.instruktor = instruktor;
  }
}
