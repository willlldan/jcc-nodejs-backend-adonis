import Kelas from "./kelas";
import Student from "./student";

export default class Bootcamp {
  constructor(name) {
    this.name = name;
    this.classes = [];
  }

  createClass(name, level, instruktor) {
    this.classes.push(new Kelas(name, level, instruktor));
  }

  register(kelas, newStudent) {
    this.classes
      .find((findClass) => findClass.name == kelas)
      .addStudent(newStudent);
  }

  runBatch() {
    this.classes.forEach((kelas) => {
      kelas.student.forEach((st, i, students) => {
        for (let j = 0; j < 4; j++) {
          var score = Math.floor(Math.random() * (100 - 50) + 50);
          st.score.push(score);
          st.finalScore += score;
          if (j == 3) {
            st.finalScore = Math.floor(st.finalScore / st.score.length);
          }
        }
      });
      console.log(`graduated from ${kelas.name} :`, this.graduate(kelas.name));
    });
  }

  graduate(kelas) {
    return this.classes.find((findClass) => findClass.name == kelas).graduate();
  }

  //   ==========================================
  // Setter & Getter
  //   ==========================================

  get bclasses() {
    return this.classes;
  }

  set bclasses(classes) {
    this.classes = classes;
  }

  get bname() {
    return this.name;
  }

  set bname(name) {
    this.name = name;
  }
}
