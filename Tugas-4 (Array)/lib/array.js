// =================================================
// Soal no 1 - Range
// =================================================

function range(num1, num2) {
  var range = [];
  if (num1 != undefined && num2 != undefined) {
    if (num1 < num2) {
      for (i = num1; i <= num2; i++) {
        range.push(i);
      }
    } else {
      for (i = num1; i >= num2; i--) {
        range.push(i);
      }
    }
  }

  return range;
}

// =================================================
// Soal no 2 - Range with step
// =================================================

function rangeWithStep(num1, num2, step = 1) {
  var range = [];
  currentStep = 0;
  if (num1 != undefined && num2 != undefined) {
    if (num1 < num2) {
      for (i = num1; i <= num2; i++) {
        if (currentStep % step == 0) range.push(i);
        currentStep++;
      }
    } else {
      for (i = num1; i >= num2; i--) {
        if (currentStep % step == 0) range.push(i);
        currentStep++;
      }
    }
  }

  return range;
}

// =================================================
// Soal no 3 - Sum of range
// =================================================

function sum(num1, num2, step = 1) {
  count = 0;
  currentStep = 0;

  if (!step) step = 1;

  if (num1 != undefined && num2 != undefined) {
    if (num1 < num2) {
      for (i = num1; i <= num2; i++) {
        if (currentStep % step == 0) count += i;
        currentStep++;
      }
    } else {
      for (i = num1; i >= num2; i--) {
        if (currentStep % step == 0) count += i;
        currentStep++;
      }
    }
  }

  if (!num2) return num1;

  return count;
}

// =================================================
// Soal no 4 - Array multidimensi
// =================================================

function dataHandling(array) {
  for (var i = 0; i < array.length; i++) {
    console.log(`Nomor ID: ${array[i][0]}`);
    console.log(`Nama Lengkap: ${array[i][1]}`);
    console.log(`TTL: ${array[i][2]} ${array[i][3]}`);
    console.log(`Hobby: ${array[i][4]}`);
    console.log("--------------------------");
  }
}

// =================================================
// Soal no 5 - Balik Kata
// =================================================

function balikKata(kata) {
  kataBalik = "";
  for (let i = kata.length - 1; i >= 0; i--) {
    kataBalik += kata[i];
  }

  return kataBalik;
}

module.exports = { range, rangeWithStep, sum, dataHandling, balikKata };
