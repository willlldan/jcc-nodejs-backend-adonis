var array = require("../lib/array");
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];
var args = process.argv;

switch (args[2]) {
  case "range":
    console.log(array.range(parseInt(args[3]), parseInt(args[4])));
    break;
  case "rangeWithStep":
    console.log(
      array.rangeWithStep(
        parseInt(args[3]),
        parseInt(args[4]),
        parseInt(args[5])
      )
    );
    break;
  case "sum":
    console.log(
      array.sum(parseInt(args[3]), parseInt(args[4]), parseInt(args[5]))
    );
    break;
  case "dataHandling":
    array.dataHandling(input);
    break;
  case "balikKata":
    console.log(array.balikKata(args[3]));
    break;
  default:
    console.log("Perintah yang anda masukan tidak sesuai");
    console.log("List functions : ");
    console.log("node array range <angka 1> <angka 2>");
    console.log("node array rangeWithStep <angka 1> <angka 2> <step>");
    console.log("node array sum <angka1> <angka2> optional:<step>");
    console.log("node array dataHandling");
    console.log(
      "node array balikKata <kata> || 'jika lebih dari satu kata pakai petik'"
    );

    break;
}
