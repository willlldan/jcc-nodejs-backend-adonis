// Soal 1 if-else

var nama = "Will";
var peran = "Warewolf";

if (nama == "") {
  console.log("Nama harus diisi !");
} else if (peran == "") {
  console.log(`Halo  ${nama}, Pilih peranmu untuk memulai game!`);
} else {
  console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
  if (peran == "Penyihir") {
    console.log(
      `Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`
    );
  } else if (peran == "Guard") {
    console.log(
      `Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf`
    );
  } else if (peran == "Warewolf") {
    console.log(
      `Halo ${peran} ${nama},  Kamu akan memakan mangsa setiap malam!`
    );
  } else {
    console.log("Peran yang kamu masukan tidak ada !!");
  }
}

console.log("=======================================================");

// ==================================== //
// Soal No 2 Switch Case
// ==================================== //

var hari = 12;
var bulan = 12;
var tahun = 2022;

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
    bulan = null;
    break;
}

if (hari > 0 && hari < 31) {
  if (bulan) {
    if (tahun > 1900 && tahun < 2200) {
      console.log(`${hari} ${bulan} ${tahun}`);
    } else {
      console.log("tahun harus diantara 1900-2200");
    }
  } else {
    console.log(`bulan tidak sesuai`);
  }
} else {
  console.log(`hari tidak sesuai, tidak ada tanggal ${hari}`);
}
