import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ActorsMovies extends BaseSchema {
  protected tableName = 'actors_movies'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('movie_id')
            .unsigned()
            .references('movies.id')
            .onDelete('cascade')
      table.integer('actor_id')
            .unsigned()
            .references('actors.id')
            .onDelete('cascade')
      

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.dateTime('created_at', { useTz: true })
      table.dateTime('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
