import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateGenreValidator from 'App/Validators/CreateGenreValidator'

export default class GenresController {
  public async index({response}: HttpContextContract) {
    let genres =await Database.from('genres').select('id', 'name')
    response.status(200).json({messages: "Success get data", data: genres})
  }

  public async store({request, response}: HttpContextContract) {
    await request.validate(CreateGenreValidator)

    try {
      await Database.table('genres').insert({
        name: request.input('name')
     })
      response.created({messages: "Successfully Added"})      
    } catch (error) {
      response.badRequest(error)
    }
  }

  public async show({params, response}: HttpContextContract) {
    let genres = await Database
                        .from('genres')
                        .where('id', params.id)
                        .select('id', 'name')
                        .firstOrFail()
    let movies = await Database
                        .from('movies')
                        .where('genre_id', params.id)
                        .select('id', 'title', 'release_date', 'resume')
    response.status(200).json({messages: "success get genres", data : {
      id : genres.id,
      genre: genres.name,
      movies
    }})
  }

  public async update({request, response, params}: HttpContextContract) {

    try {
      await Database
              .from('genres')
              .where('id', params.id)
              .update({
                name: request.input('name')
              })
    response.status(200).json({messages: "Successfully Updated!"})
    } catch (error) {
      response.badRequest(error)
    }
    

  }

  public async destroy({params, response}: HttpContextContract) {
    try {
      await Database
                .from('genres')
                .where('id', params.id)
                .delete()
    response.status(200).json({messages: "Successfully Deleted!"})
    } catch (error) {
      response.badRequest(error)
    }
  }
}
