import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateMovieValidator from 'App/Validators/CreateMovieValidator'

export default class MoviesController {
  public async index({response}: HttpContextContract) {
    let movies = await Database.from('movies').select('id', 'title', 'resume', 'release_date', 'genre_id')
    response.status(200).json({messages: "Succes get data movies", data: movies});
  }

  public async store({request, response}: HttpContextContract) {
    await request.validate(CreateMovieValidator)

    try {
      await Database.table('movies').insert({
        title: request.input('title'),
        resume: request.input('resume'),
        release_date: request.input('release_date'),
        genre_id: request.input('genre_id'),
        
     })
      response.created({messages: "Successfully Added"})      
    } catch (error) {
      response.badRequest(error)
    }
  }

  public async show({params, response}: HttpContextContract) {

    let movie = await Database
                        .from('movies')
                        .join('genres', 'movies.genre_id', 'genres.id')
                        .where('movies.id', params.id)
                        .select('movies.id', 'title', 'resume', 'release_date', 'name as genre')
                        .firstOrFail()
    response.status(200).json({messages: "success get movie", data : movie})

  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
              .from('movies')
              .where('id', params.id)
              .update({
                title: request.input('title'),
                resume: request.input('resume'),
                release_date: request.input('release_date'),
                genre_id: request.input('genre_id'),
              })
    response.status(200).json({messages: "Successfully Updated!"})
    } catch (error) {
      response.badRequest(error)
    }
  }

  public async destroy({params, response}: HttpContextContract) {
    try {
      await Database
                .from('movies')
                .where('id', params.id)
                .delete()
    response.status(200).json({messages: "Successfully Deleted!"})
    } catch (error) {
      response.badRequest(error)
    }
  }
}
