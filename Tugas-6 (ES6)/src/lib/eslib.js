// ====================================
// Soal 1
// ====================================

const sapa = (nama) => `Halo, selamat pagi, ${nama}`;

// Soal No 2

const convert = (nama, domisili, umur) => {
  return { nama, domisili, umur };
};

// Soal no 3
const checkScore = (stringData) => {
  let newArray = stringData.split(",");
  return {
    name: newArray[0].split(":")[1],
    class: newArray[1].split(":")[1],
    score: newArray[2].split(":")[1],
  };
};

// soal no 4
const filterData = (kelas) => {
  const data = [
    { name: "Ahmad", class: "adonis" },
    { name: "Regi", class: "laravel" },
    { name: "Bondra", class: "adonis" },
    { name: "Iqbal", class: "vuejs" },
    { name: "Putri", class: "Laravel" },
  ];

  return data.filter((data) => data.class.toLowerCase() == kelas.toLowerCase());
};

export const menu = (key) => {
  switch (key[2]) {
    case "sapa":
      console.log(sapa(key[3]));
      break;
    case "convert":
      console.log(convert(key[3], key[4], key[5]));
      break;
    case "checkScore":
      console.log(checkScore(key[3], key[4], key[5]));
      break;
    case "filterData":
      console.log(filterData(key[3]));
      break;
    default:
      console.log("Perintah yang anda masukan tidak sesuai");
      console.log("List functions : ");
      console.log("node dist sapa <nama>");
      console.log("node dist convert <nama> <domisili> <umur>");
      console.log("node dist checkScore <string-data>");
      console.log("node dist filterData <nama-kelas>");

      break;
  }
};
