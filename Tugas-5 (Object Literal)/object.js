// ======================================================== //
// Soal 1 - Array To Object
// ======================================================== //

function arrayToObject(people) {
  if (people == null || people.length == 0)
    return console.log("Data tidak boleh kosonng");

  var now = new Date();

  for (let i = 0; i < people.length; i++) {
    age = people[i][3]
      ? now.getFullYear() - people[i][3]
      : "Invalid Birth Year";

    person = {
      firstName: people[i][0],
      lastName: people[i][1],
      gender: people[i][2],
      age: age,
    };

    console.log(
      `${i + 1}. ${person.firstName} ${person.lastName} : ` +
        JSON.stringify(person)
    );
  }
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
// arrayToObject([]);

console.log(
  "==================================================================="
);

// ======================================================== //
// Soal 2 - Shopping Time
// ======================================================== //

function shoppingTime(memberId, money) {
  if (!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja";
  if (money < 50000) return "Mohon maaf, uang tidak cukup";

  listPurchase = [];
  listProduct = [
    ["Sepatu Stacattu", 1500000],
    ["Baju Zoro", 500000],
    ["Baju H&N", 250000],
    ["Sweater Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];

  changeMoney = money;

  for (let i = 0; i < listProduct.length; i++) {
    if (listProduct[i][1] <= changeMoney) {
      listPurchase.push(listProduct[i][0]);
      changeMoney -= listProduct[i][1];
    }
  }

  shopping = {
    memberId: memberId,
    money: money,
    listPurchase: listPurchase,
    changeMoney: changeMoney,
  };

  return shopping;
}

console.log(shoppingTime("1820RzKrnWn08", 3000000));
console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(
  "==================================================================="
);
// ======================================================== //
// Soal 3 - Naik Angkot
// ======================================================== //

function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];

  if (arrPenumpang == null || arrPenumpang.length == 0)
    return "Data tidak boleh kosonng";

  penumpang = [];

  for (let i = 0; i < arrPenumpang.length; i++) {
    ongkos = 0;
    for (
      let j = rute.indexOf(arrPenumpang[i][1]);
      j < rute.indexOf(arrPenumpang[i][2]);
      j++
    ) {
      ongkos += 2000;
    }

    penumpang.push({
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      turunDari: arrPenumpang[i][2],
      bayar: ongkos,
    });
  }

  return penumpang;
}

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);

console.log(naikAngkot());

console.log(
  "==================================================================="
);

// ======================================================== //
// Soal 4 - Nilai tertinggi
// ======================================================== //

function nilaiTertinggi(siswa) {
  var listClass = [];
  var ranking = {};

  // find list of class
  for (let i = 0; i < siswa.length; i++) {
    if (listClass.indexOf(siswa[i]["class"]) < 0)
      listClass.push(siswa[i]["class"]);
  }

  // find first ranking for everyclass
  for (let i = 0; i < listClass.length; i++) {
    var highScore = null;
    for (let j = 0; j < siswa.length; j++) {
      if (siswa[j]["class"] == listClass[i]) {
        if (highScore == null) highScore = siswa[j];
        highScore =
          highScore["score"] < siswa[j]["score"] ? siswa[j] : highScore;
      }
    }

    ranking[listClass[i]] = {
      name: highScore.name,
      score: highScore.score,
    };
  }

  return ranking;
}

// TEST CASE
console.log(
  nilaiTertinggi([
    {
      name: "Asep",
      score: 90,
      class: "adonis",
    },
    {
      name: "Ahmad",
      score: 85,
      class: "vuejs",
    },
    {
      name: "Regi",
      score: 74,
      class: "adonis",
    },
    {
      name: "Afrida",
      score: 78,
      class: "reactjs",
    },
  ])
);

console.log(
  nilaiTertinggi([
    {
      name: "Bondra",
      score: 100,
      class: "adonis",
    },
    {
      name: "Putri",
      score: 76,
      class: "laravel",
    },
    {
      name: "Iqbal",
      score: 92,
      class: "adonis",
    },
    {
      name: "Tyar",
      score: 71,
      class: "laravel",
    },
    {
      name: "Hilmy",
      score: 80,
      class: "vuejs",
    },
  ])
);
